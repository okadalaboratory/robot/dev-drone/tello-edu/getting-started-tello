# Tello EDUの始め方
![Tello EDU](https://static.dbeta.me/stormsend/uploads/ac750fe0-bd85-0136-bfc8-22000a969061/EDU_06.png "Tello")

<img src="fig/2023-10-01_15.01.22.png" width=400>

## TelloEDUとは？
> たくさんの驚きがつまったTello EDUは、プログラミングも学べるので、教育にもぴったりなドローンです。<br>
> Scratch／Python／Swiftといったプログラミング言語を簡単に学べます。<br>
> アップグレードされたSDK 2.0を搭載するTello EDUは、さらに高度なコマンドと充実したデータインターフェースを備えています。<br>
> Tello EDUは、DJIのフライトコントロール技術を採用し、EIS（電子式映像ブレ補正）にも対応。<br>
> 編隊飛行させるコマンドのコードを書いて、わくわくするAI機能を開発してみよう。<br>
> Tello EDUで、プログラミングがもっと楽しく学べます！<br>
[公式サイト](https://www.ryzerobotics.com/jp/tello-edu)（https://www.ryzerobotics.com/jp/tello-edu)より

### スペック
|        |     スペック     | 
| -------| ----------- | 
| 機体   | 重量：８７g（プロペラとバッテリを含む） <br> サイズ：98x92.5x41 mm <br>720p動画<br>マイクロUSB端子|
| 飛行性能 |最大飛行距離：１００m<br>最大スピード：8m/秒<br>最大飛行時間：13分<br>最大飛行高度:30m  |
| バッテリー | 1.1Ah/3.8V | 
| カメラ| 静止画：5MP(2592x1936)<br>画角：82.6°<br>動画：HD720P30<br>フォーマット：JPG(静止画）、MP4(動画） | 

### マニュアル
[ユーザマニュアル](https://dl-cdn.ryzerobotics.com/downloads/Tello/Tello%20User%20Manual%20v1.4.pdf)

[免責事項および安全ガイドライン](https://dl-cdn.ryzerobotics.com/downloads/Tello/20180211/Tello+Disclaimer+and+Safety+Guidelines+(EN)+v1.0.pdf)

[クイックスタートガイド](https://dl-cdn.ryzerobotics.com/downloads/Tello/20180212/Tello+Quick+Start+Guide_V1.2+multi.pdf)

[ミッションパッドの使い方](https://dl-cdn.ryzerobotics.com/downloads/Tello/Tello%20Mission%20Pad%20User%20Guide.pdf)


### 動画で知る
[公式サイトの動画](https://www.ryzerobotics.com/jp/tello-edu/videos)

### FAQ
[TelloEDU FAQ](https://www.ryzerobotics.com/jp/tello-edu/faq)

[System5 Q&A](https://info.system5.jp/dronefaq/archives/category/tello)


## 準備と片付け
- 箱から出す<br>
TelloEDUを箱から出し、付属品が揃っているかを確認します。<br>
バッテリーは箱には入っていません。

- 状態の確認<br>
プロペラやプロペラガードなど外観をチェックします。

- バッテリーを充電する<br>
次に使う時に備えて、バッテリーを充電しておきましょう。

- 箱にしまう

## Q&Aサイトを熟読する
[System5 Q&A](https://info.system5.jp/dronefaq/archives/category/tello)を熟読して、Tello EDUについて学んでください。

## 初めて使う
1. スマホにアプリをインストールする<br>
iPhone, iPadやAndroidスマートフォンのアプリストアで「tello」で検索すると、公式の無料アプリが見つかります。

2. バッテリーの装着<br>
充電が完了しているバッテリーを本体に装着します。<br>
向きを間違えないように、最後までしっかりと差し込んでください。<br>
機体によってはバッテリー装着口が少しきつい場合があります。最後までしっかりと差し込まれていることを確認してください。

3. Tello EDUを初期化する<br>
電源ボタンを5秒以上押し続けることで、Tell EDUを初期化します。

4.ファームウェア更新
Telloアプリからファームウェアを最新版にアップグレードしてください。

## スマホアプリで飛ばしてみよう
1. Tello EDUの起動
電源ボタン

2. アプリの起動

3. 離陸


## 情報収集
[Qiitaで](https://qiita.com/tags/tello)


[Telloを離陸させる前の準備を教えてください](https://info.system5.jp/dronefaq/archives/2500)


## Q&Aサイト
[System5 Q&A](https://info.system5.jp/dronefaq/archives/category/tello)
- [TelloがWi-Fi接続できない場合はどうすればいいですか？](https://info.system5.jp/dronefaq/archives/5134)
- [ランプが赤色点灯になり充電ができなくなってしまったのですがどうすればいいですか？](https://info.system5.jp/dronefaq/archives/4355)
- [DJI Assistant 2に接続した時に、違う機体が認識される場合はどうしたらいいですか？](https://info.system5.jp/dronefaq/archives/4073)
- [エラーコード205(システムオーバーヒート)が出たらどうしたらいいの？](https://info.system5.jp/dronefaq/archives/4015)
- [ファームウェアの更新に失敗する理由は何ですか？](https://info.system5.jp/dronefaq/archives/4004)
- [使用してしないときは、バッテリーをどのように保管すればいいですか？](https://info.system5.jp/dronefaq/archives/3996)
- [Telloの初期化はどうすればできますか？](https://info.system5.jp/dronefaq/archives/3809)
- [気温が高い時フライトしていると、画面がグリーン色になったり画像遅延が起きるのですが、どうすればいいですか？](https://info.system5.jp/dronefaq/archives/3697)
- [バッテリーセルエラーが出てしまった場合はどうすればいいでしょうか？](https://info.system5.jp/dronefaq/archives/3441)
- [Telloのシリアルナンバーはどこに書いてありますか？](https://info.system5.jp/dronefaq/archives/2533)
- [インテリジェントバッテリーの正しい使用方法及びメンテナンス方法を教えてください](https://info.system5.jp/dronefaq/archives/2391)
- [フライト後のバッテリーが充電できない場合はどうすればいいでしょうか？](https://info.system5.jp/dronefaq/archives/2204)




